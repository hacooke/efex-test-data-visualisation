
#ifndef FIBRE_ENERGY_CONVERTER_H
#define FIBRE_ENERGY_CONVERTER_H

#include "infraL1Calo/SignalDataType.h"

class FibreEnergyConverter {

public:
    FibreEnergyConverter( const SignalDataType& type );
    FibreEnergyConverter( const SignalDataType::SignalDataTypeEnum type
                              = SignalDataType::None );

    int encode( int val );
    int decode( int val );
    void setDataType( const SignalDataType::SignalDataTypeEnum type );

private:
    int encodeEfexLatome( int val );
    int encodeEfexTrex( int val );
    int encodeJfexLatome( int val );
    int encodeJfexTrex( int val );
    int encodeGfexLatomeCentral( int val );
    int encodeGfexLatomeForward( int val );
    int encodeGfexTrex( int val );

    int decodeEfexLatome( int val );
    int decodeEfexTrex( int val );
    int decodeJfexLatome( int val );
    int decodeJfexTrex( int val );
    int decodeGfexLatomeCentral( int val );
    int decodeGfexLatomeForward( int val );
    int decodeGfexTrex( int val );

    SignalDataType m_type;
};

#endif
