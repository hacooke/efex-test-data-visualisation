
#include "infraL1Calo/FibreEnergyConverter.h"

#include "infraL1Calo/SignalDataType.h"

/*!
 *  \class FibreEnergyConverter
 *  Encode/decode Et values between the linear scales used by eFEX/jFEX/gFEX
 *  and the various multilinear scales used to transmit them on the fibres.
 *
 *  The conversions for LAr LATOMEs are described in
 *  https://gitlab.cern.ch/atlas-lar-be-firmware/LATOME/LATOME/raw/master/LATOME/doc/LAr-LATOME-FW/LAr-LATOME-FW.pdf
 *  and those for Tile PPM-TREX are in
 *  https://edms.cern.ch/ui/file/1533071/0.2/TREX_FEX_DataFormats_v3_20191014.pdf
 */

FibreEnergyConverter::FibreEnergyConverter( const SignalDataType& type )
: m_type(type)
{
}

FibreEnergyConverter::FibreEnergyConverter( const SignalDataType::SignalDataTypeEnum type )
: m_type(type)
{
}

int
FibreEnergyConverter::encode( int val )
{
  switch( m_type.getEnum() ) {
    case SignalDataType::EfexLatome:         return encodeEfexLatome( val );
    case SignalDataType::EfexTrex:           return encodeEfexTrex( val );
    case SignalDataType::JfexLatome:         return encodeJfexLatome( val );
    case SignalDataType::JfexTrex:           return encodeJfexTrex( val );
    case SignalDataType::GfexLatomeCentral:  return encodeGfexLatomeCentral( val );
    case SignalDataType::GfexLatomeForward:  return encodeGfexLatomeForward( val );
    case SignalDataType::GfexTrex:           return encodeGfexTrex( val );
    case SignalDataType::None:               ;
  }
  return val;
}

int
FibreEnergyConverter::decode( int val )
{
  switch( m_type.getEnum() ) {
    case SignalDataType::EfexLatome:         return decodeEfexLatome( val );
    case SignalDataType::EfexTrex:           return decodeEfexTrex( val );
    case SignalDataType::JfexLatome:         return decodeJfexLatome( val );
    case SignalDataType::JfexTrex:           return decodeJfexTrex( val );
    case SignalDataType::GfexLatomeCentral:  return decodeGfexLatomeCentral( val );
    case SignalDataType::GfexLatomeForward:  return decodeGfexLatomeForward( val );
    case SignalDataType::GfexTrex:           return decodeGfexTrex( val );
    case SignalDataType::None:               ;
  }
  return val;
}

void
FibreEnergyConverter::setDataType( const SignalDataType::SignalDataTypeEnum type )
{
  m_type = SignalDataType(type);
}

int
FibreEnergyConverter::encodeEfexLatome( int val )
{
  if ( val < -31 )
    val = -31;
  for ( int i = 0 ; i < 4 ; ++i )
    if ( val < (1<<(6+2*i)) )
      return ( (val>>i)+(1<<(5+i)) );
  if ( val < 8000 )
    return ( (val>>4)+512 );
  if ( val < 44000 )
    return ( ((val-8000)>>12)+1012 ); 
  return 1023;                 // Saturated
}

int
FibreEnergyConverter::encodeEfexTrex( int val )
{
  if ( val < 0 )
    val = 0;
  if ( val > 5100 )
    val = 5100;
  return val/20;
}

int
FibreEnergyConverter::encodeJfexLatome( int val )
{
  if ( val < -127 )
    val = -127;
  if ( val > 32000 )
    val = 32000;
  for ( int i = 0 ; i < 5 ; ++i )
    if ( val < (1<<(2*i+8)) )
      return ( (val>>i)+(1<<(i+7)) );
  return 0xfff;    // Invalid, should not reach here
}

int
FibreEnergyConverter::encodeJfexTrex( int val )
{
  if ( val < 0 )
    val = 0;
  if ( val > 10200 )
    val = 10200;
  return val/40;
}

int
FibreEnergyConverter::encodeGfexLatomeCentral( int val )
{
  if ( val < -4560 )
    val = -4560;
  if ( val > 40768 )
    val = 40768;
  if ( val < -2000 )
    return ( (val+5072)/512 );
  if ( val < -512 )
    return ( (val+2012)/2 );
  if ( val < 512 )
    return ( val+1262 );
  if ( val < 2048 )
    return ( (val+3036)/2 );
  if ( val < 8000 )
    return ( (val+8120)/4 );
  return ( (val+4118720)/1024 );
}

int
FibreEnergyConverter::encodeGfexLatomeForward( int val )
{
  return encodeGfexLatomeCentral( val );
}

int
FibreEnergyConverter::encodeGfexTrex( int val )
{
  if ( val < 0 )
    val = 0;
  if ( val > 20440 )
    val = 20440;
  return val/40;
}

int
FibreEnergyConverter::decodeEfexLatome( int val )
{
  if ( val < 0 )
    val = 0;
  val &= 0x3ff;
  if ( val == 0x3ff )
    return 0xffff;
  for ( int i = 0 ; i < 4 ; ++i )
    if ( val < (96<<i) )
      return ( (val<<i)-(1<<(2*i+5)) );
  if ( val < 1012 )
    return 16*val-8192;
  return ((val-1012)<<12)+8000;
}

int
FibreEnergyConverter::decodeEfexTrex( int val )
{
  if ( val < 0 )
    val = 0;
  val &= 0xff;
  return val*20;
}

int
FibreEnergyConverter::decodeJfexLatome( int val )
{
  if ( val < 0 ) 
    val = 0;
  val &= 0xfff;
  if ( val == 0xfff )
    return 0xffff;
  for ( int i = 0 ; i < 5 ; ++i )
    if ( val < (384<<i) )
      return ( (val<<i)-(1<<(2*i+7)) );
  return 0;
}

int
FibreEnergyConverter::decodeJfexTrex( int val )
{
  if ( val < 0 )
    val = 0;
  val &= 0xff;
  return val*40;
}

int
FibreEnergyConverter::decodeGfexLatomeCentral( int val )
{
  if ( val < 0 )
    val = 0;
  val &= 0xfff;
  if ( val == 0xfff )
    return 0xffff;
  if ( val < 6 )
    return ( val*512-5072 );
  if ( val < 750 )
    return ( val*2-2012 );
  if ( val < 1774 )
    return ( val-1262 );
  if ( val < 2542 )
    return ( val*2-3036 );
  if ( val < 4030 )
    return ( val*4-8120 );
  return ( val*1024-4118720 );  
}

int
FibreEnergyConverter::decodeGfexLatomeForward( int val )
{
  return decodeGfexLatomeCentral( val );
}

int
FibreEnergyConverter::decodeGfexTrex( int val )
{
  if ( val < 0 )
    val = 0;
  val &= 0x7ff;
  return val*40;
}




