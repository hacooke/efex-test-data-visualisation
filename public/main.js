let towerArr = null
let nLayers = 5
let seedSelectStage = 0
let seedLoc = null
let seedUp = true

function renderGrid( energyData )
{
    let gridDiv = document.getElementById('GeneratedLayers')
    if ( !energyData )
    {
        gridDiv.innerHTML = ""
        return
    }
    towerArr = new TowerArray(3, 3, energyData)
    gridDiv.innerHTML = towerArr.HTML()
    autoSeedSelect()
//    console.log( towerArr.SetSeed([0]) )
//    console.log( towerArr.SetSeed([1]) )
//    console.log( towerArr.SetSeed([2]) )
//    console.log( towerArr.SetSeed([3]) )
//    console.log( towerArr.SetSeed([4]) )
//    console.log( towerArr.SetSeed([5]) )
//    console.log( towerArr.SetSeed([6]) )
//    console.log( towerArr.SetSeed([7]) )
//    console.log( towerArr.SetSeed([8]) )
}

function setLayer(layer)
{
    for (let iLayer = 0; iLayer < nLayers; iLayer++)
    {
        for ( let element of document.querySelectorAll(`.CaloLayer${iLayer}`) )
        {
            element.style.display = (iLayer == layer || layer == -1) ? 'block' : 'none'
        }
    }
    document.getElementById('layerLabels').style.display = (layer == -1) ? 'block' : 'none'
}

function seedSelectStart()
{
    resetIsoHighlights()
    if (seedSelectStage != 0)
    {
        console.log(`Seed select stage of ${seedSelectStage} in seedSelectStart`)
        return;
    }
    seedSelectStage = 1
    updateControls()
    setLayer(2)
}

// If a SuperCell is clicked on, use the location for selecting the seed,
// if in seed selection mode (determined by seedSelectStage)
function scClick( id )
{
    if (seedSelectStage < 1 || seedSelectStage > 2) return
    // Retrieve cell location from ID string
    let loc = id.match(/\d/g).map(Number)
    // ^ gives an array of three numbers, the tower, layer, and supercell
    // for the selected seed.
    // Note: These numbers are relative values for the 3x3 grid of towers,
    // not actual eta or phi coordinates
    if (seedSelectStage == 1 )
    {
        let scElem = document.getElementById(id)
        scElem.style.border = '2px solid red'
        seedLoc = loc
        seedSelectStage = 2
        updateControls()
    }
    else if (seedSelectStage == 2 )
    {
        seedUp = loc[0] <= seedLoc[0]
        let towerUpDown = seedLoc[0] + ( seedUp ? -3 : 3 )
        if ( towerUpDown >= 0 )
        {
            let elemId = `Tower${towerUpDown}CaloLayer${seedLoc[1]}SuperCell${seedLoc[2]}`
            let scElem = document.getElementById(elemId)
            scElem.style.border = '2px solid yellow'
            seedSelectStage = 3
            updateControls()
        }
    }
}

function autoSeedSelect()
{
    towerArr.SeedFinder()
    seedLoc = null
    submitSeedSelect()
}

function cancelSeedSelect()
{
    seedLoc = null
    seedUp = true
    submitSeedSelect()
}

function restartSeedSelect()
{
    cancelSeedSelect()
    seedSelectStart()
}

function submitSeedSelect()
{
    if ( seedLoc ) towerArr.SetSeed(seedLoc, seedUp)
    seedSelectStage = 0
    updateControls()
    // restore SuperCells to original state
    for ( let element of document.querySelectorAll('.SuperCell') )
    {
        element.style.border = '1px solid black'
    }
    setIsolationValues()
}

function setIsolationValues()
{
    if (!towerArr.Seeded()) return
    //Cluster energy:
    let emClus = towerArr.SumSCEnergies( towerArr.EMClusterCore() )
    let emHex = Math.floor(emClus/4.).toString(16).padStart(3,'0')
    let tobString = towerArr.IsTob() ? 'valid' : 'invalid'
    document.getElementById("EMClusterValue").innerText =
        `${renderEnergy(emClus)} (0x${emHex}) [${tobString}]`;
    //R_eta:
    let reCore = towerArr.SumSCEnergies( towerArr.RetaCore() )
    let reEnv = towerArr.SumSCEnergies( towerArr.RetaEnvironment() ) 
    document.getElementById("RetaValue").innerText =
        `${formatEnergy(reCore/reEnv)} (${renderEnergy(reCore)}/${renderEnergy(reEnv)})`;
    //R_had:
    let rhCore = towerArr.SumSCEnergies( towerArr.RhadCore() )
    let rhEnv = towerArr.SumSCEnergies( towerArr.RhadEnvironment() )
    document.getElementById("RhadValue").innerText =
        `${formatEnergy(rhCore/rhEnv)} (${renderEnergy(rhCore)}/${renderEnergy(rhEnv)})`;
    //w_s,tot:
    let wsEnv = towerArr.SumSCEnergies( towerArr.WstotEnvironment() )
    let wsOne = towerArr.SumSCEnergies( towerArr.WstotOnes() )
    let wsFour = towerArr.SumSCEnergies( towerArr.WstotFours() )
    document.getElementById("WstotValue").innerText =
        `${formatEnergy((4*wsFour+wsOne)/wsEnv)} ((4*${renderEnergy(wsFour)}+${renderEnergy(wsOne)})/${renderEnergy(wsEnv)})`;
    //Tau Cluster energy:
    let tauClus = towerArr.SumSCEnergies( towerArr.TauClusterCore() )
    let tauHex = Math.floor(tauClus/4.).toString(16).padStart(3,'0')
    document.getElementById("TauClusterValue").innerText =
        `${renderEnergy(tauClus)} (0x${tauHex})`;
    //Tau R_eta:
    let tauReCore = towerArr.SumSCEnergies( towerArr.TauRetaCore() )
    let tauReEnv = towerArr.SumSCEnergies( towerArr.TauRetaEnvironment() ) 
    document.getElementById("TauRetaValue").innerText =
        `${formatEnergy(tauReCore/tauReEnv)} (${renderEnergy(tauReCore)}/${renderEnergy(tauReEnv)})`;
    //Tau R_had:
    let tauRhCore = towerArr.SumSCEnergies( towerArr.TauRhadCore() )
    let tauRhEnv = towerArr.SumSCEnergies( towerArr.TauRhadEnvironment() )
    document.getElementById("TauRhadValue").innerText =
        `${formatEnergy(tauRhCore/tauRhEnv)} (${renderEnergy(tauRhCore)}/${renderEnergy(tauRhEnv)})`;
}

// function to toggle which controls are visible depending on the state
// of global variables (e.g. seedSelectStage)
function updateControls()
{
    //console.log(`Updating controls, seed select ${seedSelectStage}`)

    // Hide visualisation if data is still null
    document.getElementById("visualisation").style.display = 
        ( data ) ? 'block' : 'none';

    // Layer controls (incl. select seed button)
    // - visible only when not in seed select mode
    document.getElementById("LayerViewControls").style.display = 
        ( seedSelectStage == 0 ) ? 'flex' : 'none';

    // Seed select text (different in various stages of seed selection
    let sstextElem = document.getElementById("SeedSelectText")
    sstextElem.style.display = ( seedSelectStage > 0 ) ? 'flex' : 'none'
    sstextElem.innerText =
        ( seedSelectStage == 1 ) ? "Click on the highest energy SuperCell" :
        ( seedSelectStage == 2 ) ? "Click on the highest energy neighbour in phi" :
        ( seedSelectStage == 3 ) ? "Select 'Done' if correct" :
        "";

    // Seed select controls
    document.getElementById("btnSubmitSS").style.display = 
        ( seedSelectStage == 3 ) ? 'inline-block' : 'none';
    document.getElementById("SeedSelectControls").style.display = 
        ( seedSelectStage > 0 ) ? 'flex' : 'none';
}

function setIsoHighlight( index )
{
    if ( index == 0 ) resetIsoHighlights();
    else if (index == 1 ) EMClusHighlight();
    else if (index == 2 ) RetaHighlight();
    else if (index == 3 ) RhadHighlight();
    else if (index == 4 ) WstotHighlight();
    else if (index == 5 ) TauClusHighlight();
    else if (index == 6 ) TauRetaHighlight();
    else if (index == 7 ) TauRhadHighlight();
}

function resetIsoHighlights()
{
    for ( let element of document.querySelectorAll('.IsoVal') )
    {
        element.style.border = '1px solid black'
    }
    for ( let element of document.querySelectorAll('.SuperCell') )
    {
        element.style.backgroundColor = 'white'
        element.style.color = 'black'
    }
}

function EMClusHighlight()
{
    resetIsoHighlights()
    // Highlight box for info
    document.getElementById('EMClusterSum').style.border = '2px solid red'
    let coreDivList = towerArr.GetSCDivs( towerArr.EMClusterCore() )
    for ( let i=0; i < coreDivList.length; i++ )
    {
        let elemName = coreDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'lime'
    }
}

function RetaHighlight()
{
    resetIsoHighlights()
    // Highlight box for info
    document.getElementById('Reta').style.border = '2px solid red'
    let coreDivList = towerArr.GetSCDivs( towerArr.RetaCore() )
    for ( let i=0; i < coreDivList.length; i++ )
    {
        let elemName = coreDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'lime'
    }
    let envDivList = towerArr.GetSCDivs( towerArr.RetaEnvironment() )
    for ( let i=0; i < envDivList.length; i++ )
    {
        let elemName = envDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'gold'
    }
}

function RhadHighlight()
{
    resetIsoHighlights()
    // Highlight box for info
    document.getElementById('Rhad').style.border = '2px solid red'
    let coreDivList = towerArr.GetSCDivs( towerArr.RhadCore() )
    for ( let i=0; i < coreDivList.length; i++ )
    {
        let elemName = coreDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'lime'
    }
    let envDivList = towerArr.GetSCDivs( towerArr.RhadEnvironment() )
    for ( let i=0; i < envDivList.length; i++ )
    {
        let elemName = envDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'gold'
    }
}

function WstotHighlight()
{
    resetIsoHighlights()
    // Highlight box for info
    document.getElementById('Wstot').style.border = '2px solid red'
    let envDivList = towerArr.GetSCDivs( towerArr.WstotEnvironment() )
    for ( let i=0; i < envDivList.length; i++ )
    {
        let elemName = envDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'gold'
    }
    let oneDivList = towerArr.GetSCDivs( towerArr.WstotOnes() )
    for ( let i=0; i < oneDivList.length; i++ )
    {
        let elemName = oneDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'lime'
    }
    let fourDivList = towerArr.GetSCDivs( towerArr.WstotFours() )
    for ( let i=0; i < fourDivList.length; i++ )
    {
        let elemName = fourDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'green'
    }
}

function TauClusHighlight()
{
    resetIsoHighlights()
    // Highlight box for info
    document.getElementById('TauClusterSum').style.border = '2px solid red'
    let coreDivList = towerArr.GetSCDivs( towerArr.TauClusterCore() )
    for ( let i=0; i < coreDivList.length; i++ )
    {
        let elemName = coreDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'lime'
    }
}

function TauRetaHighlight()
{
    resetIsoHighlights()
    // Highlight box for info
    document.getElementById('TauReta').style.border = '2px solid red'
    let coreDivList = towerArr.GetSCDivs( towerArr.TauRetaCore() )
    for ( let i=0; i < coreDivList.length; i++ )
    {
        let elemName = coreDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'lime'
    }
    let envDivList = towerArr.GetSCDivs( towerArr.TauRetaEnvironment() )
    for ( let i=0; i < envDivList.length; i++ )
    {
        let elemName = envDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'gold'
    }
}

function TauRhadHighlight()
{
    resetIsoHighlights()
    // Highlight box for info
    document.getElementById('TauRhad').style.border = '2px solid red'
    let coreDivList = towerArr.GetSCDivs( towerArr.TauRhadCore() )
    for ( let i=0; i < coreDivList.length; i++ )
    {
        let elemName = coreDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'lime'
    }
    let envDivList = towerArr.GetSCDivs( towerArr.TauRhadEnvironment() )
    for ( let i=0; i < envDivList.length; i++ )
    {
        let elemName = envDivList[i]
        let iElem = document.getElementById( elemName )
        iElem.style.backgroundColor = 'gold'
    }
}

function formatEnergy( energy )
{
    return parseFloat( energy.toFixed(3) );
}

function renderEnergy( energy )
{
    let outEnergy = -101;
    if ( energyUnit == 0 ) outEnergy = energy;
    else if ( energyUnit == 1 ) outEnergy = energy/40;
    return outEnergy;
}

function updateRenderedEnergies()
{
    renderGrid( data )
    setIsolationValues()
}

function updateEnergyUnit( dd )
{
    energyUnit = dd.value
    updateRenderedEnergies()
}

updateControls()
renderGrid( data )
setLayer( layerDefault )
setIsoHighlight( highlightDefault )
document.getElementById( "layerdropdown" ).selectedIndex = layerDefault + 1
document.getElementById( "highlightdropdown" ).selectedIndex = highlightDefault
document.getElementById( "energydropdown" ).selectedIndex = energyUnit
document.getElementById( "energydropdown2" ).selectedIndex = energyUnit
