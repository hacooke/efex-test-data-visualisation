class TowerArray
{
    constructor( n_eta, n_phi, energyString )
    {
        this.seedTower = null
        this.seedSC = null
        this.seedUp = true
        // Read and validate string
        var lines = energyString.split('|').filter(e => e);
        if ( n_eta*n_phi != lines.length ) console.log( "ERROR in TowerArray constructor: incompatible energy string and array dimensions." );
        // Construct towers
        this.towers = [];
        for ( var i_phi = 0; i_phi < n_phi; i_phi++ )
        {
            this.towers.push([])
            for ( var i_eta = 0; i_eta < n_eta; i_eta++ )
            {
                this.towers[i_phi].push( new Tower( lines[n_phi*i_eta + i_phi] ) )
            }
        }
    }

    // Sets the seed given an 3-long array of numbers:
    // [ towerIndex (0-9), layer number (2), SuperCell number (0-3) ]
    // (the layer number is always 2 as that's where seed-finding is done )
    SetSeed( seedLocation, seedUp )
    {
        this.seedUp = seedUp
        let towerEta = seedLocation[0] % 3
        let towerPhi = Math.round( (seedLocation[0] - towerEta) / this.towers[0].length)
        this.seedTower = [towerPhi, towerEta]
        this.seedSC = seedLocation[2]
        //return [towerPhi, towerEta, this.towers[towerPhi][towerEta].layers[2].superCells[0][3].energy]
    }

    SeedFinder( )
    {
        this.seedTower = [1, 1]
        this.seedSC = this.GetTower( this.seedTower ).FindSeed()
        let aboveEnergy = this.GetTower([2,1]).GetSCEnergy(2, this.seedSC)
        let belowEnergy = this.GetTower([0,1]).GetSCEnergy(2, this.seedSC)
        //note: seed up variable is actually inverted!! (whoops -- fix this!)
        this.seedUp = !( aboveEnergy >= belowEnergy )
    }

    GetTower( index )
    {
        let towerPhi = index[0]
        let towerEta = index[1]
        return this.towers[towerPhi][towerEta]
    }

    GetSCEnergy( SCCoord )
    {
        let tower = SCCoord[0]
        let layer = SCCoord[1]
        let supercell = SCCoord[2]
        return this.GetTower( tower ).GetSCEnergy( layer, supercell )
    }

    Seeded()
    {
        return !( (!this.seedTower && this.seedTower != 0) ||
                  (!this.seedSC && this.seedSC != 0) )
    }

    IsTob()
    {
        if (!this.Seeded()) return false
        let seedCoord = [this.seedTower, 2, this.seedSC]
        let seedEnergy = this.GetSCEnergy( seedCoord )
        for ( let iEta = -1; iEta <= 1; iEta ++ )
        {
            for ( let iPhi = -1; iPhi <= 1; iPhi ++ )
            {
                if ( (iEta == 0) && (iPhi == 0) ) continue
                let iEnergy = this.GetSCEnergy(
                    this.GetRelativeSCCoord(seedCoord, iEta, iPhi)
                );
                if ( iEnergy > seedEnergy ) return false
                if ( (iEnergy == seedEnergy) && ((iEta >= 0) && (iEta > 0 || iPhi > 0)) )
                    return false;
            }
        }
        return true
    }

    SumSCEnergies( SCList )
    {
        let sum = 0
        for ( let i = 0; i < SCList.length; i++ )
        {
            let SCCoord = SCList[i]
            sum += this.GetSCEnergy( SCCoord )
        }
        return sum
    }

    GetRelativeSCCoord( SCCoord, dEta, dPhi )
    {
        // [HARD CODING 1SC ROW PER LAYER] (fine for phase 1)
        // [HARD CODING LAYER GEOMETRY]
        //SCCoord in format: [ [towerPhi, towerEta], layer, SC ]
        let layer = SCCoord[1]
        let NSCPerTower = (layer == 1 || layer == 2) ? 4 : 1
        let newSCCoord = SCCoord.slice()
        newSCCoord[0] = SCCoord[0].slice()
        // phi translation simple: change tower coordinate
        newSCCoord[0][0] = SCCoord[0][0] + dPhi
        // eta translation may or may not have to change the tower:
        let dEtaTower = Math.floor( (dEta + SCCoord[2]) / NSCPerTower )
        newSCCoord[0][1] = SCCoord[0][1] + dEtaTower
        newSCCoord[2] = ( ((dEta + SCCoord[2]) % NSCPerTower) + NSCPerTower ) % NSCPerTower
        // ^ 'double modulus' to properly handle negative numbers
        return newSCCoord
    }

    // ======= Functions to return lists of SC locations (based on seed) =======
    //Todo: add error checking for out of bounds coords
    //EM Clusters:
    EMClusterCore()
    {
        let rtn = []
        let phiMod = this.seedUp ? -1 : 1
        // layer 0
        let l0Seed = [ this.seedTower, 0, 0 ]
        rtn.push( l0Seed )
        rtn.push( this.GetRelativeSCCoord( l0Seed, 0, phiMod ) )
        // layer 1
        let l1Seed = [ this.seedTower, 1, this.seedSC ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, phiMod) )
        }
        // layer 2
        let l2Seed = [ this.seedTower, 2, this.seedSC ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, phiMod) )
        }
        // layer 3
        let l3Seed = [ this.seedTower, 3, 0 ]
        rtn.push( l3Seed )
        rtn.push( this.GetRelativeSCCoord( l3Seed, 0, phiMod ) )
        //
        return rtn
    }

    RetaCore()
    {
        let rtn = []
        let phiMod = this.seedUp ? -1 : 1
        let l2Seed = [ this.seedTower, 2, this.seedSC ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, phiMod) )
        }
        return rtn
    }

    RetaEnvironment()
    {
        let rtn = []
        let phiMod = this.seedUp ? -1 : 1
        let l2Seed = [ this.seedTower, 2, this.seedSC ]
        for ( let iEta = -3; iEta <= 3; iEta++ )
        {
            for ( let iPhi = -1; iPhi <= 1; iPhi++ )
            {
                //skip if in RetaCore
                if ( (iEta < 2 && iEta > -2) && (iPhi == 0 || iPhi == phiMod) )
                    continue;
                rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, iPhi) )
            }
        }
        return rtn
    }

    RhadCore()
    {
        let rtn = []
        let phiMod = this.seedUp ? -1 : 1
        // layer 0
        let l0Seed = [ this.seedTower, 0, 0 ]
        for ( let iPhi = -1; iPhi <= 1; iPhi++ )
        {
            rtn.push( this.GetRelativeSCCoord( l0Seed, 0, iPhi ) )
        }
        // layer 1
        let l1Seed = [ this.seedTower, 1, this.seedSC ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            for ( let iPhi = -1; iPhi <= 1; iPhi++ )
            {
                rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, iPhi) )
            }
        }
        // layer 2
        let l2Seed = [ this.seedTower, 2, this.seedSC ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            for ( let iPhi = -1; iPhi <= 1; iPhi++ )
            {
                rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, iPhi) )
            }
        }
        // layer 3
        let l3Seed = [ this.seedTower, 3, 0 ]
        for ( let iPhi = -1; iPhi <= 1; iPhi++ )
        {
            rtn.push( this.GetRelativeSCCoord( l3Seed, 0, iPhi ) )
        }
        return rtn
    }

    RhadEnvironment()
    {
        let rtn = []
        let l4Seed = [ this.seedTower, 4, 0 ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            for ( let iPhi = -1; iPhi <= 1; iPhi++ )
            {
                rtn.push( this.GetRelativeSCCoord(l4Seed, iEta, iPhi) )
            }
        }
        return rtn;
    }

    WstotOnes()
    {
        let rtn = []
        let l1Seed = [ this.seedTower, 1, this.seedSC ]
        for ( let iEta of [-1, 1] )
        {
            for ( let iPhi = -1; iPhi <= 1; iPhi++ )
            {
                rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, iPhi) )
            }
        }
        return rtn
    }

    WstotFours()
    {
        let rtn = []
        let l1Seed = [ this.seedTower, 1, this.seedSC ]
        for ( let iEta of [-2, 2] )
        {
            for ( let iPhi = -1; iPhi <= 1; iPhi++ )
            {
                rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, iPhi) )
            }
        }
        return rtn
    }

    WstotEnvironment()
    {
        let rtn = []
        let l1Seed = [ this.seedTower, 1, this.seedSC ]
        for ( let iEta = -2; iEta <= 2; iEta++ )
        {
            for ( let iPhi = -1; iPhi <= 1; iPhi++ )
            {
                rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, iPhi) )
            }
        }
        return rtn
    }

    //Tau Clusters:
    TauClusterCore()
    {
        let rtn = []
        let phiMod = this.seedUp ? -1 : 1
        // layer 0
        let l0Seed = [ this.seedTower, 0, 0 ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l0Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l0Seed, iEta, phiMod) )
        }
        // layer 1
        let l1Seed = [ this.seedTower, 1, this.seedSC ]
        for ( let iEta = -2; iEta <= 2; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, phiMod) )
        }
        // layer 2
        let l2Seed = [ this.seedTower, 2, this.seedSC ]
        for ( let iEta = -2; iEta <= 2; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, phiMod) )
        }
        // layer 3
        let l3Seed = [ this.seedTower, 3, 0 ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l3Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l3Seed, iEta, phiMod) )
        }
        // layer 4
        let l4Seed = [ this.seedTower, 4, 0 ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l4Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l4Seed, iEta, phiMod) )
        }
        //
        return rtn
    }

    TauRetaCore()
    {
        let rtn = []
        let phiMod = this.seedUp ? -1 : 1
        let l1Seed = [ this.seedTower, 2, this.seedSC ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, phiMod) )
        }
        return rtn
    }

    TauRetaEnvironment()
    {
        let rtn = []
        let phiMod = this.seedUp ? -1 : 1
        let l2Seed = [ this.seedTower, 2, this.seedSC ]
        for ( let iEta = -4; iEta <= 4; iEta++ )
        {
            //skip if in RetaCore
            if ( iEta < 2 && iEta > -2 )
                continue;
            rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, phiMod) )
        }
        return rtn
    }

    TauRhadCore()
    {
        let rtn = []
        let phiMod = this.seedUp ? -1 : 1
        let l4Seed = [ this.seedTower, 4, 0 ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l4Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l4Seed, iEta, phiMod) )
        }
        return rtn;
    }

    TauRhadEnvironment()
    {
        let rtn = []
        let phiMod = this.seedUp ? -1 : 1
        // layer 1
        let l1Seed = [ this.seedTower, 1, this.seedSC ]
        for ( let iEta = -1; iEta <= 1; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l1Seed, iEta, phiMod) )
        }
        // layer 2
        let l2Seed = [ this.seedTower, 2, this.seedSC ]
        for ( let iEta = -2; iEta <= 2; iEta++ )
        {
            rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, 0) )
            rtn.push( this.GetRelativeSCCoord(l2Seed, iEta, phiMod) )
        }
        return rtn
    }
    //==========================================================================

    HTML()
    {
        // construct divs containing all the towers
        let rtn = ""
        for ( let i_phi = 0; i_phi < this.towers.length; i_phi++ )
        {
            let phiRow = this.towers[i_phi]
            let rowHtml = `<div class="TowerRow" id="TowerRow${i_phi}">`
            for ( let i_eta = 0; i_eta < phiRow.length; i_eta++ )
            {
                let iTower = phiRow[i_eta]
                let towerNum = i_phi*phiRow.length + i_eta
                rowHtml += iTower.HTML(`Tower${towerNum}`)
            }
            rowHtml += "</div>"
            rtn += rowHtml
        }
        return rtn
    }

    GetSCDivs( SCList )
    {
        let rtn = []
        for ( let i = 0; i < SCList.length; i++ )
        {
            let SCCoord = SCList[i]
            let towerPhi = SCCoord[0][0]
            let towerEta = SCCoord[0][1]
            let layer = SCCoord[1]
            let supercell = SCCoord[2]
            let towerIndex = this.towers[0].length*towerPhi + towerEta
            rtn.push( `Tower${towerIndex}CaloLayer${layer}SuperCell${supercell}` )
        }
        return rtn
    }
}
