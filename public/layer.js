class Layer
{
    constructor( n_eta, n_phi, energies )
    {
        // NB: energies 1D list, lists energies at varying eta positions for each phi position in turn
        // (n_phi = 1 for all standard arrangements)
        // Validate dimensions
        if (n_eta*n_phi != energies.length) console.log("ERROR in Layer constructor: incompatible energies and dimensions")
        // Construct SuperCells
        this.superCells = [];
        for ( var i_phi = 0; i_phi < n_phi; i_phi++ )
        {
            this.superCells.push([])
            for ( var i_eta = 0; i_eta < n_eta; i_eta++ )
            {
                this.superCells[i_phi].push( new SuperCell( energies[n_eta*i_phi + i_eta] ) )
            }
        }
    }

    GetSCEnergy( supercell )
    {
        // NB: 1 row in phi hard-coded here!! (not generally true)
        return this.superCells[0][supercell].GetEnergy()
    }

    HTML( layerName, towerName )
    {
        let rtn = `<div class="CaloLayer ${layerName}">`
        for ( let i_phi = 0; i_phi < this.superCells.length; i_phi++ )
        {
            let SCRow = this.superCells[i_phi]
            let rowHtml = `<div class="SCRow id=SCRow${i_phi}">`
            for ( let i_eta = 0; i_eta < SCRow.length; i_eta++ )
            {
                let iSuperCell = SCRow[i_eta]
                let SCNum = i_phi*SCRow.length + i_eta
                rowHtml += iSuperCell.HTML( `SuperCell${SCNum}`, layerName, towerName )
            }
            rowHtml += "</div>"
            rtn += rowHtml
        }
        rtn += "</div>"
        return rtn
        
    }
}
