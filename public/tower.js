class Tower
{
    constructor( energyString )
    {
        // Read and check energies
        var energies = energyString.split(',');
        this.isNull = (energies.length != 11);
        // Construct layers
        this.layers = []
        if (!this.isNull)
        {
            this.layers.push( new Layer( 1, 1, energies.slice(0,1) ) )  //preshower/layer 0
            this.layers.push( new Layer( 4, 1, energies.slice(1,5) ) )  //layer 1
            this.layers.push( new Layer( 4, 1, energies.slice(5,9) ) )  //layer 2
            this.layers.push( new Layer( 1, 1, energies.slice(9,10) ) ) //layer 3
            this.layers.push( new Layer( 1, 1, energies.slice(10) ) )   //TileCal
        }
    }

    GetLayer( layer )
    {
        return this.layers[layer]
    }

    GetSCEnergy( layer, supercell )
    {
        return this.GetLayer( layer ).GetSCEnergy( supercell )
    }

    FindSeed( )
    {
        let highestEnergy = 0
        let highestSC = null
        for ( let iSC = 0; iSC < 4; iSC++ )
        {
            let iEnergy = this.GetSCEnergy(2, iSC) 
            if ( iEnergy >= highestEnergy )
            {
                highestEnergy = iEnergy
                highestSC = iSC
            }
        }
        return highestSC;
    }

    HTML( towerName )
    {
        let rtn = `<div class="Tower" id="${towerName}">`
        for ( let iLayer = 0; iLayer < this.layers.length; iLayer++ )
        {
            rtn += this.layers[iLayer].HTML(`CaloLayer${iLayer}`, towerName)
        }
        rtn += "</div>"
        return rtn
    }
}
