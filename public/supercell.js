class SuperCell
{
    constructor( energy )
    {
        this.energy = parseInt(energy)
    }

    GetEnergy()
    {
        return this.energy
    }

    HTML( scName, layerName, towerName )
    {
        let id = `${towerName}${layerName}${scName}`
        let rtn = `<div class="SuperCell ${scName}" id="${id}" onclick="scClick('${id}')">`
        rtn += `${renderEnergy(this.energy)}`
        rtn += "</div>"
        return rtn
    }
}
