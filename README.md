# eFEX Test Data Visualisation

An interactive tool to visualise energies in SuperCells, and some isolation sums, to help with debugging in testing eFEXs.

## Installing

Installation requires a C++ compiler (executed through make); node.js; and the node
package manager, npm.

1. Make a root directory for the project, and a build directory inside it
1. Clone this repository inside the root directory, this will give
   your source directory but also where the code will run from
2. Install node dependencies
3. Compile the C++ executable in the build directory using CMake

Example:
```bash
mkdir eFEX-viz{,/build} && cd $_/..
git clone https://gitlab.cern.ch/hacooke/efex-test-data-visualisation.git source
cd source
npm install
cd ../build
cmake ../source
make
```

## Running

To run the program execute `node index.js` from the source directory,
then navigate to `http://localhost:3000` in your browser.

Example (if starting from the build directory at end of installation):
```bash
cd ../source
node index.js
```
