const express = require('express')
const app = express()
const bodyparse = require('body-parser')
const { exec } = require('child_process')
//import { TowerArray } from 'TowerJS/towerarray.mjs'

app.set('view engine', 'ejs')
app.use(bodyparse.urlencoded({extended:true}))
app.use(express.static(__dirname + '/public'))

app.get('/', (req, res) => {
    res.render('index',{data:null,defaults:['BigTowers.txt',1,1,1,-1,0]})
})

app.post('/', (req, res) => {
    // fetch values
    let file = req.body.file.trim()
    let eventNum = parseInt(req.body.eventNum.trim())
    let eta = req.body.eta.trim()
    let phi = req.body.phi.trim()
    let defaultLayer = parseInt(req.body.defLayer)
    let defaultHighlight = parseInt(req.body.defHigh)
    let energyUnit = parseInt(req.body.energyUnit)
    // implement next and prev buttons
    let btnName = req.body.submit
    eventNum += (btnName == "btnNextEvent") ? 1 :
                (btnName == "btnPrevEvent") ? -1 : 0;
    // update client
    exec(`./parse ${file} ${eventNum} ${eta} ${phi}`, (err, stdout, stderr) => {
        if (err) { console.error(err) }
        else { res.render('index',{data:stdout,defaults:[file,eventNum,eta,phi,defaultLayer,defaultHighlight,energyUnit]}) }
    })
})

var server = app.listen(3000, () => {
    console.log("Visit http://localhost:3000 in your browser.")
})


