#include <string>
#include <vector>
#include <set>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <sstream>

#include "infraL1Calo/FibreEnergyConverter.h"
#include "infraL1Calo/SignalDataType.h"

using str = std::string;
//using iss = std::istringstream;

struct CLSettings {
    str file;
    size_t event;
    int eta;
    int phi;
};

void processCLArgs( const std::vector<str> &args, CLSettings &settings )
{
    size_t n{args.size()};
    if ( n != 5 ) throw std::out_of_range("Incorrect CL args");
    settings.file = args[1];
    settings.event = stoul( args[2] );
    settings.eta = stoi( args[3] );
    settings.phi = stoi( args[4] );
}

str decodeEnergy( str energy, bool isHcal=false )
{
//    (void)isHcal; return energy;
    // Convert from hex to dec:
    int encodedEnergy;
    std::stringstream ss( energy );
    ss >> std::hex >> encodedEnergy;
    // Use online software decoding:
    FibreEnergyConverter converter(
            isHcal ? 
            SignalDataType::EfexTrex :
            SignalDataType::EfexLatome
            );
    return std::to_string( converter.decode( encodedEnergy ) );
}

/*
 * Get the set of lines containing energies for the towers in a 3x3
 * arrangement centred on the provided coordinates
 *
 * \param eta eta coordinate of central tower
 * \param phi phi coordinate of central tower
 * \return set of line numbers for the towers required
 */
std::set<int> getEnvironment( int eta, int phi )
{
    std::set<int> rtn;
    for ( int iEta{eta-1}; iEta <= eta+1; iEta++ )
    {
        for ( int iPhi{phi-1}; iPhi <= phi+1; iPhi++ )
        {
            rtn.emplace( iEta*10 + iPhi );
        }
    }
    return rtn;
}

void outputTower ( str tower )
{
    std::vector<str> energies;
    std::istringstream ss(tower);
    std::copy( std::istream_iterator<str>(ss), {}, std::back_inserter(energies) );
    size_t n = energies.size();
    for( size_t i{0}; i < n-1; i++ ) printf( "%s,", decodeEnergy( energies.at(i) ).c_str() );
    printf( "%s|", decodeEnergy( energies.at(n-1), true ).c_str() );
}

int main(int argc, char *argv[])
{
    // process arguments
    const std::vector<str> clArgs{ argv, argv+argc };
    CLSettings opts{ "BigTowers.txt", 0, 0, 1 };
    processCLArgs( clArgs, opts );
    // Open file and skip to chosen event
    std::ifstream fIn( opts.file.c_str() );
    str lineRead;
    for ( size_t iEvent{0}; iEvent < opts.event; )
    {
        // read line
        std::getline(fIn, lineRead);
        // increment event if line is empty (or whitespace)
        iEvent += std::all_of(
                lineRead.begin(), lineRead.end(),
                [](unsigned char c) { return std::isspace(c); }
                );
    }
    // Grab desired towers
    int lineNum{ 0 };
    for ( int iLine : getEnvironment( opts.eta, opts.phi ) )
    {
        if ( iLine < 0 || iLine >= 60 )
        {
            // add rogue value to output?
            outputTower("0020   0020 0020 0020 0020   0020 0020 0020 0020   0020   0000");
            continue;
        }
        do std::getline(fIn, lineRead); while ( lineNum++ != iLine && lineNum <= 60);
        outputTower( lineRead );
    }
}
